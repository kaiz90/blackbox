### BLACKBOx – A Penetration Testing Framework

[![Python 2.7](https://img.shields.io/badge/python-2.7-yellow.svg)](https://www.python.org/)
[![License](https://img.shields.io/badge/license-GPLv2-red.svg)](https://bitbucket.org/darkeye/blackbox/raw/master/COPYING)
[![Twitter](https://img.shields.io/badge/twitter-@blackeye-blue.svg)](https://twitter.com/0x676)
[![Facebook](https://img.shields.io/badge/facebook-@blackeye-blue.svg)](https://www.facebook.com/blackeye.gov)
#### Password Attacks: 
+ ######MD5 CRACKER
+ ######SHA1  CRACKER
+ ######SHA224 CRACKER
+ ######SHA256 CRACKER
+ ######SHA384 CRACKER
+ ######SHA512 CRACKER
+ ######MSSQL2000 CRACKER
+ ######MSSQL2005 CRACKER
+ ######MYSQL323 CRACKER
+ ######MYSQL41 CRACKER
+ ######ORACLE11 CRACKER

#### Web Hacking :
+ ######Wordpress Bruteforce – Bruteforce wordpress panel
+ ######FTP Bruteforce       – Bruteforcing FTP LOGIN
+ ######SSH Bruteforce       – Bruteforcing SSH LOGIN
+ ######Admin Page Finder    – Find Admin Page
+ ######Prestashop Exploit   – PrestaShop Arbitrary file Upload (6 Modules)
+ ######Dnsinfo              – dns info via Yougetsignal & viewdns & hackertarget
+ ######Magento Rce          – Magento eCommerce - Remote Code Execution
+ ######Joomla  Rce          – 1.5 - 3.4.5 remote code execution
+ ######Google Dorker        – google dorker (LFI SCANNER)
+ ######Bing Dorker          – bing dorker (LFI SCANNER )
+ ######Prestashop Exploit   – PrestaShop Arbitrary file Upload (6 Modules)

####Installation :
```bash
$ git clone https://darkeye@bitbucket.org/darkeye/blackbox.git && cd blackbox && chmod +x install && sudo ./install && cd
```
##Version :
--------------------------------------
###1.4v:
+ ######Add Prestashop Exploit
+ ######Add Admin Page finder 
+ ######Add FTP Bruteforcer
+ ######Add SSH Bruteforcer
+ ######Fix google dorker
--------------------------------------
###1.5v:
+ ######Fix LFI Scanner for google & bing dorker
+ ######Fix some python error 
+ ######Fix Admin page finder
+ ######Fix Hash cracker
###1.6v:
+ ######Fix Google Dorker
+ ######Add MSSQL2000, MSSQL2005, MYSQL323, MYSQL41, ORACLE11 CRACKER